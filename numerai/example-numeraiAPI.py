#!/usr/bin/env python

from numerapi.numerapi import NumerAPI
import json

with open('./keys.json') as file:
    config = json.load(file)

def main():
    # set example username and round
    example_public_id = config["numerai"]["pub_key"]
    example_secret_key = config["numerai"]["secret_key"]

    # some API calls do not require logging in
    napi = NumerAPI(verbosity="info")
    # download current dataset
    napi.download_current_dataset(unzip=True)
    # get competitions
    all_competitions = napi.get_competitions()
    print("all_competitions", all_competitions)
    # get leaderboard for the current round
    leaderboard = napi.get_leaderboard()
    print("leaderboard", leaderboard)
    # leaderboard for a historic round
    leaderboard_67 = napi.get_leaderboard(round_num=67)
    print("leaderboard_67", leaderboard_67)

    # provide api tokens
    napi = NumerAPI(example_public_id, example_secret_key)
    print("napi", napi)


    # upload predictions
    submission_id = napi.upload_predictions("mypredictions.csv")
    # check submission status
    napi.submission_status()


if __name__ == "__main__":
    main()
