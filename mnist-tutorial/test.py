from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

import tensorflow as tf

x = tf.placeholder(tf.float32, [None, 784])
W = tf.Variable(tf.zeros([784, 20]))
W2 = tf.Variable(tf.zeros([20, 10]))
b = tf.Variable(tf.ones([10]))
b2 = tf.Variable(tf.ones([20]))
r = tf.matmul(tf.matmul(x, W) + b2, W2) + b
print(r)
y = tf.nn.softmax(r)

y_ = tf.placeholder(tf.float32, [None, 10])

cross_entropy = tf.reduce_mean(tf.square(y_-y))
# cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))

# train_step = tf.train.AdamOptimizer(0.5).minimize(cross_entropy)
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

sess = tf.InteractiveSession()

tf.global_variables_initializer().run()

correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))

accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

for _ in range(1000):
  batch_xs, batch_ys = mnist.train.next_batch(1000)
  _, acc = sess.run([train_step, accuracy], feed_dict={x: batch_xs, y_: batch_ys})
  print(acc)



# batch_xs, batch_ys = mnist.test.next_batch(5000)

print(sess.run(accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
